Feature: Trigger Put API
@Put_API_TestCases
Scenario: Trigger the put API request with valid request parameters
		Given Enter NAME and JOB in put request body
		When Send the put request with payload to certain resource
		Then Validate put Status code
		And Validate put response body parameters