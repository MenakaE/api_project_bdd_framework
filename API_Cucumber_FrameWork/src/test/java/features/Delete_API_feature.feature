Feature: Trigger Delete API
@Delete_API_TestCases
Scenario: Trigger the Delete API
		Given Enter Delete endpoint
		When Send the Delete API request
		Then Validate Delete Status code