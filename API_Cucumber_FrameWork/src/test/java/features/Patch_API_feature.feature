Feature: Trigger Patch API
@Patch_API_TestCases
Scenario: Trigger the Patch API request with valid request parameters
		Given Enter NAME and JOB in patch request body
		When Send the Patch request with payload
		Then Validate Patch status code
		And Validate Patch response Body parameters  