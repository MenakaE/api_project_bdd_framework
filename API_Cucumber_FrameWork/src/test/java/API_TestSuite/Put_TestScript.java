package API_TestSuite;

import java.time.LocalDateTime;

import org.testng.Assert;
import io.restassured.path.json.JsonPath;

public class Put_TestScript{

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, exp_date);
	}
}
