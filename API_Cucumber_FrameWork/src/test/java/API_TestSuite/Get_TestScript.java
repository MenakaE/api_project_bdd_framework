package API_TestSuite;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import io.restassured.path.json.JsonPath;

public class Get_TestScript {

	public static void validator(String responseBody) {

		int id[] = { 7, 8, 9, 10, 11, 12 };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

		JsonPath jsp_res = new JsonPath(responseBody);
		JSONObject Array_res = new JSONObject(responseBody);
		JSONArray dataArray = Array_res.getJSONArray("data");

		int count = dataArray.length();
		// System.out.println(count);

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_firstname = firstname[i];
			String exp_lastname = lastname[i];

			int res_id = dataArray.getJSONObject(i).getInt("id");
			String res_email = dataArray.getJSONObject(i).getString("email");
			String res_firstname = dataArray.getJSONObject(i).getString("first_name");
			String res_lastname = dataArray.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
		}
	}
}
