package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/data_driven_features", glue = {
		"data_driven_step_Definition" }, tags = "@Post_API_TestCases or @Put_API_TestCases")
public class TestRunner_Data_Driven {

}