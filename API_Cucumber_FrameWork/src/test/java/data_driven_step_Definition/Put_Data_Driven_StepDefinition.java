package data_driven_step_Definition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Put_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Put_endpoint;
import Request_Repository.Put_Request_Repo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Put_Data_Driven_StepDefinition {
	File logdir;
	String endpoint;
	String requestBody;
	int statuscode;
	String responseBody;
	@Given("Enter {string} and {string} in Put requestbody")
	public void enter_and_in_put_requestbody(String req_name, String req_job) {
		logdir=Create_directory.Create_log_directory("Put_API_logs");
	    endpoint=Put_endpoint.put_endpoint_test1();
	    requestBody=Put_Request_Repo.put_request(req_name, req_job);
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the data_driven_Put request with payload")
	public void send_the_data_driven_put_request_with_payload() {
		statuscode=Common_method_handle_API.put_statusCode(requestBody, endpoint);
	    responseBody=Common_method_handle_API.put_responseBody(requestBody, endpoint);
	    System.out.println(responseBody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_put status code")
	public void validate_data_driven_put_status_code() {
		Assert.assertEquals(statuscode, 200);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_put response body parameters")
	public void validate_data_driven_put_response_body_parameters() throws IOException {
		Put_TestScript.validator(requestBody, responseBody);
	    Handle_API_logs.evidence_creation(logdir, "Put_TestScipt", endpoint, requestBody, responseBody);
	    System.out.println("Put API response validation successful \n");
	    //throw new io.cucumber.java.PendingException();
	}

}
