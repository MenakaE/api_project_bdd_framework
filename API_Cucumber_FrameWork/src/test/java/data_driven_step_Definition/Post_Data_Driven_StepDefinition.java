package data_driven_step_Definition;

import java.io.File;
import java.io.IOException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Post_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Post_endpoint;
import Request_Repository.Post_Request_Repo;

public class Post_Data_Driven_StepDefinition {
	File log_dir;
	String endpoint;
	String requestBody;
	int statuscode;
	String responseBody;

	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) {
		log_dir = Create_directory.Create_log_directory("Post_API_logs");
		endpoint = Post_endpoint.post_endpoint_test1();
		requestBody = Post_Request_Repo.post_request(req_name, req_job);
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the post request with data")
	public void send_the_post_request_with_data() {
		statuscode = Common_method_handle_API.post_statusCode(requestBody, endpoint);
		responseBody = Common_method_handle_API.post_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate data_driven_post status code")
	public void validate_data_driven_post_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate data_driven_post response body parameters")
	public void validate_data_driven_post_response_body_parameters() throws IOException {
		Post_TestScript.validator(requestBody, responseBody);
		Handle_API_logs.evidence_creation(log_dir, "Post_TestScript", endpoint, requestBody, responseBody);
		System.out.println("Post Response body validation successful \n");
		// throw new io.cucumber.java.PendingException();
	}
}
