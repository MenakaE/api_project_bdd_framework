package data_driven_step_Definition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Patch_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Patch_endpoint;
import Request_Repository.Patch_Request_Repo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_Data_Driven_StepDefinition {
	File logdir;
	String endpoint;
	String requestbody;
	int statusCode;
	String responseBody;
	@Given("Enter {string} and {string} in Patch requestbody")
	public void enter_and_in_patch_requestbody(String req_name, String req_job) {
		logdir=Create_directory.Create_log_directory("Patch_API_logs");
	    endpoint=Patch_endpoint.patch_endpoint_test1();
	    requestbody=Patch_Request_Repo.patch_request(req_name, req_job);
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the data_driven_Patch request with payload")
	public void send_the_data_driven_patch_request_with_payload() {
		statusCode=Common_method_handle_API.patch_statusCode(requestbody, endpoint);
	    responseBody=Common_method_handle_API.patch_responseBody(requestbody, endpoint);
	    System.out.println(responseBody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_patch status code")
	public void validate_data_driven_patch_status_code() {
		Assert.assertEquals(statusCode, 200);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_patch response body parameters")
	public void validate_data_driven_patch_response_body_parameters() throws IOException {
	    Patch_TestScript.validator(requestbody, responseBody);
	    Handle_API_logs.evidence_creation(logdir, "Patch_TestScipt", endpoint, requestbody, responseBody);
	    System.out.println("patch API response validation successful \n");
	    //throw new io.cucumber.java.PendingException();
	}
}
