package Common_Utility_Methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Extractor {
	public static ArrayList<String> Read_Excel_Data(String filename, String Sheetname, String testname) throws IOException {

		ArrayList<String> Arraydata = new ArrayList<String>();
		String project_Directory = System.getProperty("user.dir");

		// Step 1 - Create the object of file input stream to locate the data file
		FileInputStream FIS = new FileInputStream(project_Directory + "\\API_Data_File\\"+filename+".xlsx");

		// Step 2 - Create the XSSFWorkbook object to open the excel file
		XSSFWorkbook WorkBook = new XSSFWorkbook(FIS);

		// Step 3 - Fetch the number of sheet available in excel file
		int count = WorkBook.getNumberOfSheets();

		// Step 4 - Access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheetName = WorkBook.getSheetName(i);

			if (sheetName.equals(Sheetname)) {
				System.out.println(sheetName);
				XSSFSheet sheet = WorkBook.getSheetAt(i);

				Iterator<Row> row = sheet.iterator();
				row.next();

				while (row.hasNext()) {
					Row dataRow = row.next();
					String TC_Name = dataRow.getCell(0).getStringCellValue();
					//System.out.println(TC_Name);
					
					if (TC_Name.equals(testname)) {
						Iterator<Cell> cellvalues = dataRow.iterator();
						
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							//System.out.println(testdata);
							Arraydata.add(testdata);

						}
					}
				}
				break;
			}
		}
		WorkBook.close();
		return Arraydata;
	}


}
