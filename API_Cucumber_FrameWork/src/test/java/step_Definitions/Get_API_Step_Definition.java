package step_Definitions;

import java.io.File;
import java.io.IOException;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Get_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Get_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class Get_API_Step_Definition {
	File logdir;
	String endpoint;
	String responseBody;
	int statuscode;

	@Given("Enter Get endpoint")
	public void enter_get_endpoint() {
		logdir = Create_directory.Create_log_directory("Get_API_logs");
		endpoint = Get_endpoint.get_endpoint_test1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Get API request")
	public void send_the_get_api_request() {
		statuscode = Common_method_handle_API.get_statusCode(endpoint);
		responseBody = Common_method_handle_API.get_responseBody(endpoint);
		System.out.println(responseBody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get status code")
	public void validate_get_status_code() {
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get response body parameters")
	public void validate_get_response_body_parameters() throws IOException {
		Get_TestScript.validator(responseBody);
		Handle_API_logs.evidence_creation(logdir, "Get_TestScript", endpoint, responseBody);
		System.out.println("Get Response body validation successful \n");
		// throw new io.cucumber.java.PendingException();
	}
}
