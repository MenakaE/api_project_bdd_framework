package step_Definitions;

import java.io.File;
import java.io.IOException;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Post_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Post_endpoint;
import Request_Repository.Post_Request_Repo;

public class Post_API_StepDefinition {
	File logdir;
	String endpoint;
	String requestBody;
	int statuscode;
	String responseBody;
	
	/*@Before
	public void test_setup() {
		System.out.println("Trigger API");
	}

	@After
	public void teardown() {
		System.out.println("API triggered \n");
	}*/
	
	@Given("Enter NAME and JOB in post request body")
	public void enter_name_and_job_in_post_request_body() throws IOException {
		logdir = Create_directory.Create_log_directory("Post_API_logs");
		endpoint = Post_endpoint.post_endpoint_test1();
		requestBody = Post_Request_Repo.post_request_test1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the post request with payload")
	public void send_the_post_request_with_payload() {
		statuscode = Common_method_handle_API.post_statusCode(requestBody, endpoint);
		responseBody = Common_method_handle_API.post_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate post response body parameters")
	public void validate_post_response_body_parameters() throws IOException {
		Post_TestScript.validator(requestBody, responseBody);
		Handle_API_logs.evidence_creation(logdir, "Post_TestScipt", endpoint, requestBody, responseBody);
		System.out.println("Post API response validation successful");
		// throw new io.cucumber.java.PendingException();
	}
}
