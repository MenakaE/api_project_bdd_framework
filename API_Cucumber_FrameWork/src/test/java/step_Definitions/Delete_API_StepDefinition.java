package step_Definitions;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Delete_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

public class Delete_API_StepDefinition {
	File logdir;
	String endpoint;
	int statusCode;
	
	@Given("Enter Delete endpoint")
	public void enter_delete_endpoint() {
		logdir=Create_directory.Create_log_directory("Delete_API_logs");
	    endpoint=Delete_endpoint.delete_endpoint_test1();
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the Delete API request")
	public void send_the_delete_api_request() {
	    statusCode=Common_method_handle_API.delete_statusCode(endpoint);
	    System.out.println("Delete Status code : "+statusCode);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Delete Status code")
	public void validate_delete_status_code() throws IOException {
	    Assert.assertEquals(statusCode, 204);
	    Handle_API_logs.evidence_creation(logdir, "Delete_TestScript", endpoint);
	    System.out.println("Delete API validation successful ");
	    //throw new io.cucumber.java.PendingException();
	}
}
