package step_Definitions;

import java.io.File;
import java.io.IOException;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Patch_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Patch_endpoint;
import Request_Repository.Patch_Request_Repo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class Patch_API_StepDefinition {
	File logdir;
	String endpoint;
	String requestbody;
	int statusCode;
	String responseBody;

	@Given("Enter NAME and JOB in patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		logdir = Create_directory.Create_log_directory("Patch_API_logs");
		endpoint = Patch_endpoint.patch_endpoint_test1();
		requestbody = Patch_Request_Repo.patch_request_test1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Patch request with payload")
	public void send_the_patch_request_with_payload() {
		statusCode = Common_method_handle_API.patch_statusCode(requestbody, endpoint);
		responseBody = Common_method_handle_API.patch_responseBody(requestbody, endpoint);
		System.out.println(responseBody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statusCode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Patch response Body parameters")
	public void validate_patch_response_body_parameters() throws IOException {
		Patch_TestScript.validator(requestbody, responseBody);
		Handle_API_logs.evidence_creation(logdir, "Patch_TestScipt", endpoint, requestbody, responseBody);
		System.out.println("patch API response validation successful");
		// throw new io.cucumber.java.PendingException();
	}
}
