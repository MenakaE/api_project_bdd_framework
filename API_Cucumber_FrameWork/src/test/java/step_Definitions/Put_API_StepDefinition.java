package step_Definitions;

import java.io.File;
import java.io.IOException;

import API_Common_Methods.Common_method_handle_API;
import API_TestSuite.Put_TestScript;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Put_endpoint;
import Request_Repository.Put_Request_Repo;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class Put_API_StepDefinition {
	File logdir;
	String endpoint;
	String requestBody;
	int statuscode;
	String responseBody;
	
	@Before("@Post_API_TestCases or @Put_API_TestCases")
	public void test_setup() {
		System.out.println("Trigger API");
	}

	/*@After("@Post_API_TestCases or @Put_API_TestCases")
	public void teardown() {
		System.out.println("API triggered \n");
	}*/
	
	@Given("Enter NAME and JOB in put request body")
	public void enter_name_and_job_in_put_request_body() throws IOException {
		logdir = Create_directory.Create_log_directory("Put_API_logs");
		endpoint = Put_endpoint.put_endpoint_test1();
		requestBody = Put_Request_Repo.put_request_test1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the put request with payload to certain resource")
	public void send_the_put_request_with_payload_to_certain_resource() {
		statuscode = Common_method_handle_API.put_statusCode(requestBody, endpoint);
		responseBody = Common_method_handle_API.put_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate put Status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate put response body parameters")
	public void validate_put_response_body_parameters() throws IOException {
		Put_TestScript.validator(requestBody, responseBody);
		Handle_API_logs.evidence_creation(logdir, "Put_TestScipt", endpoint, requestBody, responseBody);
		System.out.println("Put API response validation successful");
		// throw new io.cucumber.java.PendingException();
	}
}
