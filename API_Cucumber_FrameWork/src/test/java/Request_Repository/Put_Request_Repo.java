package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Methods.Excel_Data_Extractor;

public class Put_Request_Repo {
	
	public static String put_request_test1() throws IOException {
		ArrayList<String> data =Excel_Data_Extractor.Read_Excel_Data("Test_Data_API", "Put_API", "Put_TC2");
		String name=data.get(1);
		String job=data.get(2);
		String requestBody ="{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}";
		return requestBody;
	}
	
	public static String put_request(String name, String job) {
		String requestBody="{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}";
		return requestBody;
	}

}
