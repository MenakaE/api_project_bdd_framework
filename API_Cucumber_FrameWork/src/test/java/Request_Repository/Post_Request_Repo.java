package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Methods.Excel_Data_Extractor;

public class Post_Request_Repo {
	
	public static String post_request_test1() throws IOException {
		ArrayList<String> data =Excel_Data_Extractor.Read_Excel_Data("Test_Data_API", "Post_API", "Post_TC2");
		String name=data.get(1);
		String job=data.get(2);
		String requestBody = "{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}" ;
		return requestBody;
	}
	public static String post_request(String req_name, String req_job) {
		String request_body="{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		return request_body;
	}

}
