Feature: Trigger Patch API
@Patch_API_TestCases
Scenario Outline: Trigger Patch API with valid parameters
		Given Enter "<Name>" and "<Job>" in Patch requestbody
		When Send the data_driven_Patch request with payload
		Then Validate data_driven_patch status code
		And Validate data_driven_patch response body parameters
		
Examples:
		|Name |Job |
		|Rithalya|Mgr|
		|Vijay|PO|
		|Harini|SQL|