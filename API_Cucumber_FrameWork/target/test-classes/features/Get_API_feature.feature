Feature: Trigger Get API
@Get_API_TestCases
Scenario: Trigger the Get API and Validate the parameters
		Given Enter Get endpoint
		When Send the Get API request
		Then Validate Get status code
		And Validate Get response body parameters
		