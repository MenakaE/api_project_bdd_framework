Feature: Trigger Post_API
@Post_API_TestCases
Scenario: Trigger the post API request with valid request parameters
		Given Enter NAME and JOB in post request body
		When Send the post request with payload
		Then Validate post status code
		And Validate post response body parameters
