Feature: Trigger Put API
@Put_API_TestCases
Scenario Outline: Trigger Put API with valid parameters
		Given Enter "<Name>" and "<Job>" in Put requestbody
		When Send the data_driven_Put request with payload
		Then Validate data_driven_put status code
		And Validate data_driven_put response body parameters
		
Examples:
		|Name |Job |
		|Rithalya|Mgr|
		|Vijay|PO|
		|Harini|SQL|