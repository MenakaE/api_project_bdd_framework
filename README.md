# API_Project_BDD_Framework

**About Project** 

Framework designed to trigger and validate API automatically using **Behaviour driven development(BDD)**.

**BDD** - Behaviour Driven Development(BDD) is s software development methodology, which advocates the use of easy to understand language (Gherkin) and standard format(Cucumber) to design the application, that allows to cascade information betweeen all stakeholders.

**Cucumber** - Cucumber is the Automation Testing Framework that supports Behaviour Driven Development(BDD). Cucumber reads the plain English text in feature file and maps with functional code in the Step definition.

**Gherkin** - Domain specific language created mainly for behaviour descriptions. It is a plain test English language.

My framework can be classified into 6 components

1. Test Driving Mechanism
2. Feature File
3. Step Definition File
4. Common Functions
5. Data Driven Testing
6. Libraries

**1. Test Driving Mechanism**
	
- Test Runner class is used to run the tests with **@CucumberOptions** and **@RunWith** annoatations. The test runner file should contain path of the feature file and step definition file that we want to execute.
- **Maven surefire plugin** is added, can able to execute the test case without any IDE.

**2. Feature File**
	

- File is written in plain English text (**Gherkin** language). It contains requirements, which includes features, scenarios and description of tests. It uses keywords of Given, When, Then, And to define the steps of test scenario.
- **Tags** - Tags are given to execute specific test case from feature file.

**3. Step Defintion File**
	
- File contains the **functional code** that should be executed for each step of test scenario. This functional code map the corresponding test steps in the feature file.
- **Hooks** - The block of code defined in the method with **@Before**  and **@ After** annotations, that run before and after each test scenario. 

**4. Common Functions** - Common functions,which is called in Step definition for test execution.

**API related Common Functions**

1. Endpoint Repository - To store all endpoints of API
2. Request Repository - Contains request payload of each API
3. Common Function - Contains functions to trigger an API and to capture status code and response body

**Utilities related common function**

1. Create Directory Utility - To create a directory for log files if it doesn't exist. If already exists, to delete and create directory
2. Create logfile Utility - Once API is executed, creates a log file and adds the corresponding endpoint, request payload, response payload and headers into a text file(log file) using Filewriter class.
3. Excel Data Extractor Utility -  To read data or variables from excel file using apache poi library

**5. Data Driven Testing**

1. **Parameterization with example keyword** 


- Scenario Outline- It is to run the scenario with different test data, which is defined in Examples attribute. 
- Examples - Contains test data to be passed in the scenario

2. **Excel data file**-  Consists of test data, which are read from an excel file for request payload to test API

**6. Libraries** - 
Dependency management of libraries are handled by maven repository.
Libraries used in my framework are

1. Cucumber - Create wrapper on functional Java code and maps with plain text description
2. RestAssured - To trigger an API and to capture response
3. Jsonpath - To parse the response
4. Testng - For Validation of response using Assert class
5. Apache poi - To read and write data from an excel file
6. Java generic library - Used for handling exceptions, to write log files

**Added Files** - Added all my framework design related files into GitLab with the command line of GitBash with the following commands:

	git clone [https_link]
	git init
	git checkout -b "My_Checkin_API"
	git add *
	git status
	git commit -m "Adding files into repository"
	git push origin My_Checkin_API
